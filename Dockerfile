FROM ubuntu

WORKDIR /hwip_discord_bot
ADD .env main.py requirements.txt /hwip_discord_bot/
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install python3 -y
RUN apt-get install python3-pip -y
RUN apt-get install git -y
RUN DEBIAN_FRONTEND=noninteractive apt-get install ffmpeg -yq
RUN pip install -r requirements.txt

CMD ["python3", "main.py"]
